close all
clear all
l = load('iddata-15.mat');
u = l.id.u;
y = l.id.y;
id = l.id;
val = l.val;

%introducerea ordinelor
m = input('m = ');
na = input('na = ');
nb = input('nb = ');
%nk este considerat egal cu 1

%graficele cu datele initiale
subplot(211);
plot(id.u);
title('id.u');

subplot(212);
plot(id.y);
title('id.y');

figure 

subplot(211);
plot(val.u);
title('val.u');

subplot(212);
plot(val.y);
title('val.y');

%IDENTIFICARE

%generarea numerelor in baza m+1, intr-o matrice
n = na+nb;
M = [];
M = [zeros(1, n);M];
for i=1:2^(m+1)
   str = dec2base(i,m+1); 
   if length(str)<=n
       num = str2num(str);
       V = zeros(1,n);
       d = n;
        for j=length(str):-1:1
           V(d) = int64(mod(num,10));
           d = d-1;
           num = num/10;
        end
        if sum(V)<=m
         M = [M;V];
        end
   end 
end

linie = length(M(:,1));
coloana = n;

%adunarea cu +1 in matricea de puteri, pe coloane, si construirea matriceiM
M2 = [];
Mc = [];
for i=1:na+nb
  M2 = [;M]; 
  for j=1:linie
      M2(j,i) = M2(j,i)+1;
  end
  Mc = [Mc;M2];
 end

M = unique(Mc,'rows');

linie2 = length(M(:,1));
coloana = n;

y = [zeros(na+nb,1); id.y];
u = [zeros(na+nb,1); id.u];

%aflarea vectorului d(k) si a matricei PHI
N = length(id.y);
aux = 1;
PHI = [];
for k=na+1:N+na
   phi = [];
   for k1=1:na
        d(k1) = y(k-k1);
   end
   for k1=1:nb
        d(na+k1) = u(k-k1);
   end
   
    for j=1:linie2
        aux = 1;
        for i=1:na+nb
             aux = aux*(d(i)^(M(j,i)));
        end
        phi = [phi,aux];
    end
    PHI = [PHI;phi];
end

%aflarea coeficientilor theta
theta = PHI\id.y;

%identificare - vectorul ypred de predictie
ypred = PHI*theta; 
MSE_predI = 0;
MSE_predI = (sum((ypred-id.y).^2))/length(ypred-id.y);


%simulare pe datele de identificare
d = zeros(na+nb, 1);
y_sim = zeros(na, 1);
u1 = l.id.u;
y1 = l.id.y;
u = [zeros(na+nb, 1); u1];
PHI_sim=[];
c = 1;
for k=na+1:length(id.u)+na
    phi_sim = [];
    d_simY = y_sim(k-1:-1:k-na);
    d_simU = u(k-1:-1:k-nb);
    d = [d_simY', d_simU'];
    
    for j=1:linie2
        aux = 1;
        for i=1:length(d)
                aux = aux*(d(i)^(M(j,i)));
        end
        phi_sim = [phi_sim,aux];
    end
    PHI_sim = [PHI_sim; phi_sim];
    aux2 = PHI(c,:)*theta;
    y_sim = [y_sim; aux2];
    c = c+1;
end

y = y(na:length(u));
Ysim = PHI_sim*theta;

%eroarea in funcie de simulare
MSE_simI = 0;
MSE_simI = (sum((Ysim-id.y).^2))/length(Ysim-id.y);

figure
plot(id.y) %iesirile initiale
hold on
plot(ypred, 'r') %y predictie
hold on
plot(Ysim, 'g') %y simulare
legend('yid','ypred','ysim');
title({'Identificare';['MSEpred= ', num2str(MSE_predI)];['MSEsim= ', num2str(MSE_simI)]});

%VALIDARE
y_val = [zeros(na+nb,1); val.y];
u_val = [zeros(na+nb,1); val.u];

%aflarea vectorului d si a matricei PHI de validare
N_val = length(val.y);
aux = 1;
PHI_val = [];
for k=na+1:N_val+na
   phi_val = [];
  for k1=1:na
        d(k1) = y_val(k-k1);
   end
   for k1=1:nb
        d(na+k1) = u(k-k1);
   end
 
    for j=1:linie2
        aux = 1;
        for i=1:na+nb
            aux = aux*(d(i)^(M(j,i)));
        end
        phi_val = [phi_val,aux];
    end
    PHI_val = [PHI_val;phi_val];
end

 %vectorul y predictie pt datele de validare
y_predV = PHI_val*theta; 
MSE_predV=(sum((y_predV-val.y).^2))/length(y_predV-val.y);

% simulare pe datele de validare
PHI_simV=[];
u1 = l.val.u;
y1 = l.val.y;
y_simV = zeros(na+1, 1);
u = [zeros(na+nb,1); u1];
d_simY = [];
d_simU = [];
c = 1;
for k=na+1:length(val.u)+na
   phi_sim = [];
   d_simY = y_simV(k-1:-1:k-na);
   d_simU = u(k-1:-1:k-nb);
   d = [d_simY', d_simU'];
   
    for j=1:linie2
        aux=1;
        for i=1:length(d)
            aux=aux*(d(i)^(M(j,i)));
        end
        phi_sim=[phi_sim,aux];
    end
    PHI_simV=[PHI_simV;phi_sim];
    aux = PHI_val(c,:)*theta;
    y_simV = [y_simV; aux];
    c = c+1;
end

YsimV=PHI_simV*theta;

%eroarea in functie de simulare
MSE_simV = 0;
MSE_simV = (sum((YsimV-val.y).^2))/length(YsimV-val.y);

figure
plot(val.y);
hold on
plot(y_predV, 'r');
hold on
plot(y_simV, 'g')
legend('yval','ypred', 'ysim');
title({'Validare';['MSEpred= ', num2str(MSE_predV)];['  MSEsim= ', num2str(MSE_simV)]});


function x_c=x_calc(x)

x_c=zeros(2,length(x)*length(x));
x1=[];
x2=[];
for i=1:length(x)
    for j=1:length(x)
        x1=[x1,x(j)];
        x2=[x2,x(i)];
    end
end
x_c=[x1;x2];

end
function phi=coeficienti(xc,x,m)


j=1;
nr=length(x)*length(x);

for i=1:nr
    for l=0:m
        for k=0:l
            phi(i,j)=(xc(1,i)^(k))*(xc(2,i)^(l-k));
            j=j+1;
        end
    end
    j=1;
end
end
close all;
clear all;
clc;

load('proj_fit_37');
x=id.X{1};
y=id.X{2};
z=id.Y;

m=input('Gradul polinomului este: ');

mesh(x,y,z');
title('Date initiale');

yc=reshape(z,1,length(z)*length(z));
xc=x_calc(x);
phi=coeficienti(xc,x,m);

teta=phi\yc';
Y=phi*teta;
e=yc'-Y;
MSE1=(sum(e.^2))/length(e);

figure
mesh(x,y,z');
hold on
mesh(x,y,reshape(Y,length(x),length(y))','edgecolor','r');
title(['MSE1=',num2str(MSE1)]);


%validare
x_val=val.X{1};
y_val=val.X{2};
z_val=val.Y;

figure
mesh(x_val,y_val,z_val');
title('Date validare');

yc_val=reshape(z_val,1,length(z_val)*length(z_val));
xc_val=x_calc(x_val);
phi_val=coeficienti(xc_val,x_val,m);

Y_val=phi_val*teta;
e_val=yc_val'-Y_val;
MSE2=(sum(e_val.^2))/length(e_val);

figure
mesh(x_val,y_val,z_val');
hold on
mesh(x_val,y_val,reshape(Y_val,length(x_val),length(y_val))','edgecolor','r');
title(['MSE2=',num2str(MSE2)]);

MSE = [MSE1, MSE2];
t=1:m;
plot(t, MSE);
title('Evolutia MSE');
xlabel('Grad'); ylabel('MSE');

